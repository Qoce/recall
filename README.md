Synchrony mod which adds the reacll spell as a new spell item which creates a temporary one use one way portal.

Expected behavior:
The spell has two states, active and inactive.
When you pick up the item it starts inactive.
Casting the spell while it is inactive will mark a tile as a one way destination portal,
and then the spell will switch to an active state.
When the spell is cast while active it teleports the user to the portal, destroys the portal, and sets the spell to incative.
This starts a 25 kill cooldown before the spell can be active again.

A change of level automatically deactives the spell if it is active and sets the cooldown to 25 kills.
The portal is visible to every player, but it is just visual and cannot be interacted with accept by the owner of the spell.

If the tile the portal is on is occupied when the player casts recall to teleport back to it it will place the player on
the nearest unoccupied spot.
