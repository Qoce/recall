--[[
Adds Recall Spell to Necrodancer as a new item which creates a temporary one use one way portal.

Expected behavior:
The spell has two states, active and inactive.
When you pick up the item it starts inactive.
Casting the spell while it is inactive will mark a tile as a one way destination portal,
and then the spell will switch to an active state.
When the spell is cast while active it teleports the user to the portal, destroys the portal, and sets the spell to incative.
This starts a 25 kill cooldown before the spell can be active again.

A change of level automatically deactives the spell if it is active and sets the cooldown to 25 kills.
The portal is visible to every player, but it is just visual and cannot be interacted with accept by the owner of the spell.

Not Implemented Yet:
    Portals belonging to other players will show up as less bright than your own portal.
    If you walk close to a portal belonging to another player it will show their name.

If the tile the portal is on is occupied when the player casts recall to teleport back to it it will place the player on
the nearest unoccupied spot.
]]--


local event = require "necro.event.Event"
local customEntities = require "necro.game.data.CustomEntities"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local move = require "necro.game.system.Move"
local components = require "necro.game.data.Components"
local field = components.field
local inventory = require "necro.game.item.Inventory"
local entities = require "system.game.Entities"
local spellItem = require "necro.game.item.SpellItem"
local object = require "necro.game.object.Object"
local tile = require "necro.game.tile.Tile"
local collision = require "necro.game.tile.Collision"
local map = require "necro.game.object.Map"
local damage = require "necro.game.system.Damage"
local attack = require "necro.game.character.Attack"

components.register{
    spellcastRecall = {},
    recallTile = {
        field.int8("x", 0),
        field.int8("y", 0),
        field.bool("active", false),
        field.entityID("portalEntityID", nil),
    },
    recallPortal = {
        field.entityID("spellItemID"),
        field.entityID("spellCasterID")
    }
}
customEntities.extend {
    name = "SpellRecall",
    template = customEntities.template.item(),

    data = {
        flyaway = "Recall",
        hint = "Teleport Back",
        slot = "spell"
    },
    components = {
        itemCastOnUse = {
            spell  = "Recall_SpellcastRecall"
        },
        sprite = {
            texture = "mods/Recall/gfx/recallSpell.png"
        },
        Recall_recallTile = { --Store the position of the tile we will recall to in the item
            x = 0,
            y = 0,
            active = false
        },
        spellCooldownKills = {
            cooldown = 15
        },
        spellBloodMagic = {
            damage = 2
        }
    }
}

--Disables cooldown completely when the recall tile is active
--This is because this spell needs to be casted twice (once to place a tile, once to teleport) before it should go on cooldown
event.spellItemCooldown.add("onlyCooldownRecallWhenUsedToTeleport", {order = "kills", filter = "Recall_recallTile", sequence = 1}, function(ev)
    if ev.entity.Recall_recallTile.active then
        ev.cooldowns = {}
    end
end)

commonSpell.registerSpell("SpellcastRecall", {
    spellcast = {},
    spellcastProcessImmediately = {},
    soundSpellcast = {"spellGeneral"},
    friendlyName = {name = "Recall"},
    Recall_spellcastRecall = {}
})

--When the level is changed, if the portal is active destroy it and put the spell on cooldown
event.gameStateLevel.add("resetPortal", "statusEffects", function (ev)
    for entity in entities.entitiesWithComponents {"Recall_recallTile"} do
        if entity.Recall_recallTile.active then
            entity.Recall_recallTile.active = false
            entity.spellCooldownKills.remainingKills = entity.spellCooldownKills.cooldown
            spellItem.updateOpacity(entity)
        end
    end
end)

--Disabled in favor of just teleporting players to the nearest open tile
--[[event.turn.add("checkPortalObstruction", {sequence = 1, filter = "Recall_recallTile", order = "missedBeat"}, function(ev)
    --every turn check if the portal is obstructed and if it is delete the portal and set a cooldown on the spell
    dbg(ev)
    local spellitem = ev.entity
    if tile.get(spellitem.Recall_recallTile.x, spellitem.Recall_recallTile.y) == tile.EMPTY then
        spellitem.Recall_recallTile.active = false
        entities.despawn(spellitem.Recall_recallTile.portalEntityID)
    end
end)]]--

event.spellcast.add("spellcastRecall", {order = "teleport", sequence = 1, filter = "Recall_spellcastRecall"}, function(ev)
    local spellitem
    for i,id in pairs(inventory.getItems(ev.caster)) do
        local item = entities.getEntityByID(id)
        if item:hasComponent("Recall_recallTile") then
            spellitem = item
            break
        end
    end
    if ev.caster ~= nil and spellitem ~= nil then
        if spellitem.Recall_recallTile.active then
            if tile.get(spellitem.Recall_recallTile.x, spellitem.Recall_recallTile.y) ~= tile.EMPTY then
                --telefrag everything on the tile
                local targets = map.getAll(spellitem.Recall_recallTile.x,  spellitem.Recall_recallTile.y)
                for i,t in pairs(targets) do
                    local target = entities.getEntityByID(t)
                    if target ~= nil and attack.isAttackable(ev.caster, target, attack.mask(attack.Flag.PHASING, attack.Flag.DIRECT)) then
                            damage.inflict{
                                attacker = ev.caster,
                                victim = target,
                                damage = 999,
                                penetration = damage.Penetration.PHASING
                            }
                    end
                end
                --Teleport player to the recall tile or the nearest vacant tile
                --TODO: Should these two blocks in here be switched? This might be able to telefrag stuff on a tile that is different from where it ends up teleporting in some edge cases...
                local x,y = collision.findNearbyVacantTile(spellitem.Recall_recallTile.x,  spellitem.Recall_recallTile.y, collision.Group.SOLID)
                if (ev.caster.position.x - spellitem.Recall_recallTile.x)^2 + (ev.caster.position.y - spellitem.Recall_recallTile.y)^2 < (x-spellitem.Recall_recallTile.x)^2 + (y-spellitem.Recall_recallTile.y)^2 then
                    x,y = spellitem.Recall_recallTile.x, spellitem.Recall_recallTile.y
                end
                move.absolute(ev.caster, x, y, move.Type.NONE)
            end
            --desteroy recall tile
            spellitem.Recall_recallTile.active = false
            object.delete(entities.getEntityByID(spellitem.Recall_recallTile.portalEntityID))
        else
            --place recall tile
            spellitem.Recall_recallTile.x = ev.caster.position.x
            spellitem.Recall_recallTile.y = ev.caster.position.y
            spellitem.Recall_recallTile.active = true
            local portalEntity = object.spawn("Recall_RecallPortalEntity", ev.caster.position.x, ev.caster.position.y)
            spellitem.Recall_recallTile.portalEntityID = portalEntity.id
            portalEntity.Recall_recallPortal.spellItemID = spellitem.id
            portalEntity.Recall_recallPortal.spellCasterID = ev.caster.id
        end
    end
end)

customEntities.register {
    name = "RecallPortalEntity",
    gameObject = {},
    sprite = {
        texture = "mods/Recall/gfx/returnTilePlayer.png",
        width = 24,
        height = 24
    },
    position = {y = 0},
    visibility = {},
    positionalSprite = {
        offsetY = 12, --appear with tile in floor
        offsetX = 0
    },
    rowOrder = {
        z = -20 --lol so -10 wasn't low enough this seems just right 
    },
    Recall_recallPortal = {}
}